//
//  NSNumber+IOSComparsions.m
//  NavigationApp
//
//  Created by Alex on 18.03.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "NSNumber+IOSComparsions.h"

@implementation NSNumber (IOSComparsions)

- (BOOL)isGreaterThan:(NSNumber *)obj
{
    NSParameterAssert([obj isKindOfClass:[NSNumber class]]);
    return [self compare:obj] == NSOrderedDescending;
}

- (BOOL)isLessThan:(NSNumber *)obj
{
    NSParameterAssert([obj isKindOfClass:[NSNumber class]]);
    return [self compare:obj] == NSOrderedAscending;
}


#pragma mark - Number Type

- (CFNumberType)numberType
{
    return CFNumberGetType((CFNumberRef)self);
}

#pragma mark - Quick Sum

+ (NSNumber *)sumOfNumbers:(NSArray *)numbers
{
    NSNumber *sum    = nil;
    NSUInteger count = numbers.count;

    sum = @([[numbers firstObject] doubleValue]);
    for (NSUInteger i = 1; i < count; i++) {
        sum = @([sum doubleValue] + [numbers[i] doubleValue]);
    }
    
    return sum;
}


@end
