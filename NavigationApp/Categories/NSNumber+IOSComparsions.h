//
//  NSNumber+IOSComparsions.h
//  NavigationApp
//
//  Created by Alex on 18.03.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (IOSComparsions)

- (BOOL) isGreaterThan:(NSNumber *)obj;
- (BOOL) isLessThan:(NSNumber *)obj;
+ (NSNumber *)sumOfNumbers:(NSArray *)numbers;
- (CFNumberType)numberType;
@end
