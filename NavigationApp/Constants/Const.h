//
//  Const.h
//  NavigationApp
//
//  Created by Alex on 01.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#ifndef Const_h
#define Const_h


#pragma mark - GoogleAPI


#define kGoogleApiKey @"AIzaSyCxlxZhahEt2yhUiEnc6sgDvg4H7HUomQk"


#pragma mark - MapViewController


#define kDefaultLatitude    48.9445015
#define kDefaultLongtitude  38.5027704
#define kDefaultCameraZoom  13


#pragma mark - GraphDataManager


#define kSourceFileName         @"sever_lines"
#define kSourceFileExtension    @"geojson"
#define kFeatures               @"features"
#define kGeometry               @"geometry"
#define kCoordinates            @"coordinates"
#define kWeight                 @"weight"
#define kToIndex                @"toIndex"
#define kIndex                  @"index"
#define kAdjacentEdges          @"adjacentEdges"
#define kGraphFileName          @"graph.json"


#pragma mark - GoogleAPIManager


#define kTitle                  @"title"
#define kSnippet                @"snippet"
#define kRows                   @"rows"
#define kElements               @"elements"
#define kDistance               @"distance"
#define kText                   @"text"
#define kDuration               @"duration"


#pragma mark - MapPresenter


#define kTransportModeDriving   @"driving"
#define kTransportModeWalkng    @"walking"


#pragma mark - LoadingViewController


#define kAuthorText             @"tsonev alex"
#define kDescriptionText        @"severodonetsk navigation"




#endif /* Const_h */
