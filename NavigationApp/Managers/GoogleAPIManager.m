//
//  GoogleManager.m
//  NavigationApp
//
//  Created by Alex on 01.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "GoogleAPIManager.h"
#import "NetworkManager.h"
#import "Const.h"

@implementation GoogleAPIManager

+ (GMSMarker *)markerWithCoordinate:(CLLocationCoordinate2D)coordinate {
    GMSMarker *marker = [GMSMarker new];
    marker.position = coordinate;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    [[NetworkManager sharedManager] downloadDescriptionForPlaceWithCoordinate:coordinate
                                                           andCompletionBlock:^(NSDictionary *json) {
                                                               NSDictionary *info= [GoogleAPIManager markerInfoWithJson:json];
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   marker.title = info[kTitle];
                                                                   marker.snippet = info[kSnippet];
                                                               });
                                                           }];
    return marker;
}

+ (NSDictionary *)descriptionForPathWithStartCoordinate:(CLLocationCoordinate2D)startCoordinate
                                          endCoordinate:(CLLocationCoordinate2D)endCoordinate
                                                andMode:(NSString *)mode {
    NSMutableDictionary *response = [NSMutableDictionary dictionary];
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [[NetworkManager sharedManager] downloadDescriptionForPathWithStartCoordinate:startCoordinate
                                                                        endCoordinate:endCoordinate
                                                                                 mode:mode
                                                                   andCompletionBlock:^(NSDictionary *json) {
                                                                       NSDictionary *elements = json[kRows][0][kElements][0];
                                                                       NSString *dist = elements[kDistance][kText];
                                                                       NSString *dur = elements[kDuration][kText];
                                                                       
                                                                       
                                                                       [response setObject:dist
                                                                                    forKey:kDistance];
                                                                       [response setObject:dur
                                                                                    forKey:kDuration];
                                                                       dispatch_semaphore_signal(sema);
                                                                   }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    return response;
}


+ (NSDictionary *)markerInfoWithJson:(NSDictionary *)json {
    NSArray *addressComponents;
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    NSString *street_number, *route, *locality;
    
    for (NSDictionary *dict in json[@"results"]) {
        for (NSString *type in dict[@"types"]) {
            if([type isEqualToString:@"premise"] || [type isEqualToString:@"street_address"]) {
                addressComponents = dict[@"address_components"];
                for (NSDictionary *component in addressComponents) {
                    for (NSString *type in component[@"types"]) {
                        if ([type isEqualToString:@"street_number"]) {
                            street_number = component[@"long_name"];
                        }
                        else if ([type isEqualToString:@"route"]) {
                            route = component[@"short_name"];
                        }
                        else if ([type isEqualToString:@"locality"]) {
                            locality = component[@"short_name"];
                        }
                    }
                }
            }
            else if([type isEqualToString:@"route"]){
                addressComponents = dict[@"address_components"];
                for (NSDictionary *component in addressComponents) {
                    for (NSString *type in component[@"types"]) {
                        if ([type isEqualToString:@"route"]) {
                            route = component[@"short_name"];
                        }
                        else if ([type isEqualToString:@"locality"]) {
                            locality = component[@"short_name"];
                        }
                    }
                }
            }
        }
    }
    
    NSString *title;
    if(street_number) {
        title = [NSString stringWithFormat:@"%@, %@",route, street_number];
    }
    else {
        title = [NSString stringWithFormat:@"%@",route];
    }
    NSString *snippet = [NSString stringWithFormat:@"%@",locality];
    [result setObject:title forKey:@"title"];
    [result setObject:snippet forKey:@"snippet"];
    return result;
}



@end
