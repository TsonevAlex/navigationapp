//
//  GoogleManager.h
//  NavigationApp
//
//  Created by Alex on 01.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMaps;

@class Graph;
@interface GoogleAPIManager : NSObject

+ (GMSMarker *)     markerWithCoordinate:(CLLocationCoordinate2D)coordinate;
+ (NSDictionary *)  descriptionForPathWithStartCoordinate:(CLLocationCoordinate2D)startCoordinate
                                            endCoordinate:(CLLocationCoordinate2D)endCoordinate
                                                  andMode:(NSString *)mode;

@end
