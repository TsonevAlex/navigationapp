//
//  NetworkManager.m
//  SourcesAndArticles
//
//  Created by Alex on 04.02.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "NetworkManager.h"
#import "Const.h"

@interface NetworkManager ()

@property (strong, nonatomic) NSURLSession *session;

@end

@implementation NetworkManager


#pragma mark - Life Cycle


- (instancetype)init {
    self = [super init];
    
    if(self) {
        [self setupSession];
    }
    
    return self;
}

- (void)setupSession {
    self.session = [NSURLSession sessionWithConfiguration: [NSURLSessionConfiguration defaultSessionConfiguration]];
}

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static NetworkManager *sharedManager;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [NetworkManager new];
    });
    
    return sharedManager;
}


#pragma mark - Distance Matrix API


-(void)downloadDescriptionForPathWithStartCoordinate:(CLLocationCoordinate2D)start
                                       endCoordinate:(CLLocationCoordinate2D)end
                                                mode:(NSString *)mode
                                  andCompletionBlock:(void (^)(NSDictionary *json))completionBlock {
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&language=ru&mode=%@&key=%@",
                           start.latitude,
                           start.longitude,
                           end.latitude,
                           end.longitude,
                           mode,
                           kGoogleApiKey];
    NSURL *url = [NSURL URLWithString: urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *dataTask = [self.session
                                      dataTaskWithRequest:urlRequest
                                      completionHandler:^(NSData * _Nullable data,
                                                          NSURLResponse * _Nullable response,
                                                          NSError * _Nullable error) {
                                          if(data) {
                                              NSDictionary *json = [NSJSONSerialization
                                                                    JSONObjectWithData: data
                                                                    options: NSJSONReadingAllowFragments
                                                                    error: &error];
                                              completionBlock(json);
                                          }
                                          else if (error) {
                                              NSLog(@"NetworkManager Error: %@",error);
                                          }
                                      }];
    [dataTask resume];
}


#pragma mark - Geocoding API


- (void)downloadDescriptionForPlaceWithCoordinate:(CLLocationCoordinate2D)coordinate
                               andCompletionBlock:(void (^)(NSDictionary* json)) completionBlock {
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&language=%@&key=%@",coordinate.latitude,coordinate.longitude,@"ru",kGoogleApiKey];
    NSURL *url = [NSURL URLWithString: urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL: url];
    NSURLSessionDataTask *dataTask = [self.session
                                      dataTaskWithRequest:urlRequest
                                      completionHandler:^(NSData * _Nullable data,
                                                          NSURLResponse * _Nullable response,
                                                          NSError * _Nullable error) {
                                          if(data) {
                                              NSDictionary *json = [NSJSONSerialization
                                                                    JSONObjectWithData: data
                                                                    options: kNilOptions
                                                                    error: &error];
                                              completionBlock(json);
                                          }
                                          else if (error) {
                                              NSLog(@"NetworkManager Error: %@",error);
                                          }
                                      }];
    [dataTask resume];
}

+ (void)imageWithURL:(NSURL *)url
             success:(void (^)(UIImage *image))success
             failure:(void (^)(NSError *error))failure {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                       downloadTaskWithURL:url
                                                       completionHandler:^(NSURL *location,
                                                                           NSURLResponse *response,
                                                                           NSError *error) {
                                                           if (error) {
                                                               return failure(error);
                                                           }
                                                           if (response) {
                                                               UIImage *avatar = [UIImage imageWithData:
                                                                                  [NSData dataWithContentsOfURL:location]];
                                                               success(avatar);
                                                           }
                                                       }];
        
        [downloadPhotoTask resume];
    });
}




@end
