//
//  NetworkManager.h
//  SourcesAndArticles
//
//  Created by Alex on 04.02.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface NetworkManager : NSObject

+ (instancetype)sharedManager;
+ (void)        imageWithURL:(NSURL *)url
                     success:(void (^)(UIImage *image))success
                     failure:(void (^)(NSError *error))failure;

- (void)        downloadDescriptionForPlaceWithCoordinate:(CLLocationCoordinate2D)coordinate
                                       andCompletionBlock:(void (^)(NSDictionary* json))completionBlock;

- (void)        downloadDescriptionForPathWithStartCoordinate:(CLLocationCoordinate2D)start
                                                endCoordinate:(CLLocationCoordinate2D)end
                                                         mode:(NSString *)mode
                                           andCompletionBlock:(void (^)(NSDictionary* json))completionBlock;



@end
