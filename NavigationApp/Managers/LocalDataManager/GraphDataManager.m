//
//  GraphDataManager.m
//  NavigationApp
//
//  Created by Alex on 04.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "GraphDataManager.h"
#import "Graph.h"
#import "Edge.h"
#import "Vertex.h"
#import "Const.h"

@implementation GraphDataManager

+ (Graph *)calculateWithJson {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:kSourceFileName
                                                         ofType:kSourceFileExtension];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options: 0
                                                           error:&error];
    Graph *graph = [Graph new];
    NSDictionary *features = json[kFeatures];
    for (NSDictionary *item in features) {
        
        NSArray *coords = item[kGeometry][kCoordinates];
        Vertex *currentVertex = nil;
        Vertex *nextVertex = nil;
        Vertex *prevVertex = nil;
        
        for (int i = 0; i < coords.count; i++) {
            
            if (i!=0) {
                prevVertex = currentVertex;
                currentVertex = nextVertex;
            }
            else {
                currentVertex = [graph existingVertexWithCoordinates:coords[i]];
                nextVertex = nil;
                prevVertex = nil;
            }
            
            if(!currentVertex) {
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([coords[i][1] doubleValue],
                                                                               [coords[i][0] doubleValue]);
                currentVertex = [[Vertex alloc] initWithCoordinates:coordinate];
                [graph.vertices addObject:currentVertex];
                currentVertex.index = graph.vertices.count - 1;
                NSLog(@"%lu",(unsigned long)graph.vertices.count);
            }
            
            if(i == 0) {
                nextVertex = [graph existingVertexWithCoordinates:coords[i+1]];
                if(!nextVertex) {
                    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([coords[i+1][1] doubleValue],
                                                                                   [coords[i+1][0] doubleValue]);
                    nextVertex = [[Vertex alloc] initWithCoordinates:coordinate];
                    [graph.vertices addObject:nextVertex];
                    nextVertex.index = graph.vertices.count - 1;
                    NSLog(@"%lu",(unsigned long)graph.vertices.count);
                    
                }
                [currentVertex addEdgeToVertex:nextVertex];
            }
            else if(i == coords.count - 1) {
                [currentVertex addEdgeToVertex:prevVertex];
                
            }
            else {
                nextVertex = [graph existingVertexWithCoordinates:coords[i+1]];
                if(!nextVertex) {
                    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([coords[i+1][1] doubleValue],
                                                                                   [coords[i+1][0] doubleValue]);
                    nextVertex = [[Vertex alloc] initWithCoordinates:coordinate];
                    [graph.vertices addObject:nextVertex];
                    nextVertex.index = graph.vertices.count - 1;
                    NSLog(@"%lu",(unsigned long)graph.vertices.count);
                    
                }
                [currentVertex addEdgeToVertex:nextVertex];
                [currentVertex addEdgeToVertex:prevVertex];
                
            }
        }
    }
    
    return graph;
}

+ (void)writeToJson:(Graph *)graph withCompletion:(void (^)())completionBlock {
    NSArray *vertices = graph.vertices;
    NSMutableArray *graphArr = [NSMutableArray new];
    for (Vertex *vertex in vertices)
    {
        NSMutableArray *edges = [NSMutableArray new];
        NSDictionary *edgeDict;
        for (Edge *edge in vertex.adjacentEdges)
        {
            edgeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                        @(edge.adjacentTo.index), kToIndex,
                        edge.weight, kWeight,
                        nil];
            [edges addObject:edgeDict];
        }
        NSString* lat = [NSString stringWithFormat:@"%f",vertex.coordinates.latitude];
        NSString* longt = [NSString stringWithFormat:@"%f",vertex.coordinates.longitude];
        NSArray *vertexCoordinates = @[longt,lat];
        NSDictionary *vertexDictionary = [NSDictionary dictionaryWithObjects:@[vertexCoordinates,
                                                                               @(vertex.index),
                                                                               edges]
                                                                     forKeys:@[kCoordinates,
                                                                               kIndex,
                                                                               kAdjacentEdges]];
        [graphArr addObject:vertexDictionary];
    }
    
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = kGraphFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    NSError *error = nil;
    NSData *graphData = [NSJSONSerialization dataWithJSONObject:graphArr
                                                        options:NSJSONWritingPrettyPrinted
                                                          error:&error];
    [graphData writeToFile:fileAtPath atomically:YES];
    completionBlock();
}

+ (Graph *)readFromJson {
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = kGraphFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    NSData *data = [NSData dataWithContentsOfFile:fileAtPath];
    NSError *error =  nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:&error];
    NSMutableArray *vertices = [NSMutableArray array];
    for (NSDictionary *vertexDict in json) {
        NSArray *coordinates = vertexDict[kCoordinates];
        Float64 longtitude = [coordinates[0] doubleValue];
        Float64 latitude = [coordinates[1] doubleValue];
        Vertex *vert = [[Vertex alloc] initWithCoordinates:CLLocationCoordinate2DMake(latitude, longtitude)];
        vert.index = [vertexDict[kIndex] unsignedIntegerValue];
        [vertices addObject:vert];
    }
    
    for (NSDictionary *vertexDict in json) {
        NSUInteger index = [vertexDict[kIndex] unsignedIntegerValue];
        Vertex *sourceVertex = [vertices objectAtIndex:index];
        for (NSDictionary *edgeDict in vertexDict[kAdjacentEdges]) {
            NSUInteger edgeIndex = [edgeDict[kToIndex] unsignedIntegerValue];
            Vertex *toVertex = [vertices objectAtIndex:edgeIndex];
            Edge *edge = [[Edge alloc] initWithAdjacentFrom:sourceVertex
                                                         To:toVertex
                                                  andWeight:edgeDict[kWeight]];
            [sourceVertex.adjacentEdges addObject:edge];
        }
    }
    Graph *graph = [[Graph alloc] init];
    graph.vertices = vertices;
    return graph;
}

+ (BOOL)isGraphExists {
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = kGraphFileName;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:fileAtPath];
    return exists;
}

@end
