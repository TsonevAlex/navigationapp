//
//  GraphDataManager.h
//  NavigationApp
//
//  Created by Alex on 04.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Graph;
@interface GraphDataManager : NSObject

+ (Graph *) readFromJson;
+ (Graph *) calculateWithJson;
+ (void)    writeToJson:(Graph *)graph withCompletion:(void (^)())completionBlock;
+ (BOOL)    isGraphExists;
@end
