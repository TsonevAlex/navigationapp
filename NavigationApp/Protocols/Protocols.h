//
//  Protocols.h
//  NavigationApp
//
//  Created by Alex on 21.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#ifndef Protocols_h
#define Protocols_h

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class GMSPolyline, GMSMarker;
@protocol MapViewProtocol;

#pragma mark - MapPresenterProtocol

@protocol MapPresenterProtocol <NSObject>

@property (strong, nonatomic) id<MapViewProtocol> viewController;

- (void)putMarkerAtCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)drawPolylineWithStartCoordinate:(CLLocationCoordinate2D)start endCoordinate:(CLLocationCoordinate2D)end;
- (void)updateInfoWithStartCoordinate:(CLLocationCoordinate2D)start endCoordinate:(CLLocationCoordinate2D)end;

@end


#pragma mark - MapViewProtocol


@protocol MapViewProtocol <NSObject>

@property (strong, nonatomic) id<MapPresenterProtocol> presenter;
@property (assign, nonatomic, readonly) NSInteger transportMode;

- (void)drawPolyline:(GMSPolyline *)polyline;
- (void)updateInfoWithTime:(NSString *)time distance:(NSString *)distance;
- (void)setMarker:(GMSMarker *)marker;

@end


#pragma mark - InfoBarDelegate


@protocol InfoBarDelegate <NSObject>

- (void)transportModeChanged;

@end


#endif /* Protocols_h */
