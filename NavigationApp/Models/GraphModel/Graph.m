//
//  PathFinder.m
//  NavigationApp
//
//  Created by Alex on 11.02.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "Graph.h"
#import "GraphDataManager.h"
#import "Vertex.h"
#import "Edge.h"
#import "NSNumber+IOSComparsions.h"

@interface Graph ()


//AStar unwatched vertices
@property (strong, nonatomic) NSMutableArray *openList;

@end

@implementation Graph


#pragma mark - Properties


- (NSMutableArray *)vertices {
    if(!_vertices) {
        _vertices = [[NSMutableArray alloc] init];
    }
    return _vertices;
}


#pragma mark - Dijkstra


- (NSArray *)dijkstraShortestPathFromVertex:(Vertex *)aSource toVertex:(Vertex *)aTarget {
    // Preparing and initializing
    Vertex *sourceVertex = aSource;
    Vertex *targetVertex = aTarget;
    
    NSMutableArray *dist     = [@[] mutableCopy];
    NSMutableArray *previous = [@[] mutableCopy];
    NSMutableArray *Q             = self.vertices;
    
    
    for (Vertex *vertex in Q) {
        [dist insertObject:@INT_MAX atIndex:vertex.index];
        [previous insertObject:[NSNull null] atIndex:vertex.index];
    }
    [dist replaceObjectAtIndex:sourceVertex.index withObject:@0];
    
    // Finding the shortest path
    while (Q.count > [self visitedVertices].count) {
        Vertex *currentVertex = [Graph hasMinimumDistance:dist inVertices:Q];
        if (currentVertex == targetVertex) {
            break;
        }
        NSLog(@"%ld", (long)currentVertex.index);
        if ([[dist objectAtIndex:currentVertex.index] isEqual:@INT_MAX]) {
            break;
        }
        for (Edge *edge in currentVertex.adjacentEdges) {
            Vertex *v   = edge.adjacentTo;
            NSNumber *alt = [NSNumber sumOfNumbers:@[edge.weight,[dist objectAtIndex:currentVertex.index]]];
            if ([alt isLessThan:[dist objectAtIndex:v.index]]) {
                [dist replaceObjectAtIndex:v.index withObject:alt];
                [previous replaceObjectAtIndex:v.index withObject:currentVertex];
            }
        }
        currentVertex.wasVisited = YES;
    }
    
    //Returning indexes array
    NSMutableArray *indexes = [NSMutableArray array];
    [self printPathWithSource:sourceVertex
                       target:targetVertex.index
                previousArray:previous
                 indexesArray:indexes];
    
    [self resetVertices];
    return indexes;
}

- (void)printPathWithSource:(Vertex *)sourceVertex
                     target:(NSInteger)current
              previousArray:(NSArray *)previous
               indexesArray:(NSMutableArray *)indexes {
    if(current == sourceVertex.index) {
        NSLog(@"Start node --> %ld", current);
        [indexes addObject:[NSNumber numberWithInteger:current]];
        return;
    }
    NSInteger index = ((Vertex *)previous[current]).index;
    [self printPathWithSource:sourceVertex
                       target:index
                previousArray:previous
                 indexesArray:indexes];
    [indexes addObject:[NSNumber numberWithInteger:current]];
    NSLog(@"next node --> %ld", current);
}

- (void)resetVertices {
    for (Vertex *vertex in self.vertices) {
        vertex.wasVisited = NO;
        vertex.parent = nil;
    }
}

- (NSArray *)visitedVertices
{
    NSMutableArray *visited = [@[] mutableCopy];
    
    for (Vertex *vertex in self.vertices) {
        if (vertex.wasVisited) {
            [visited addObject:vertex];
        }
    }
    
    return visited;
}

+ (Vertex *)hasMinimumDistance:(NSMutableArray *)aDist inVertices:(NSArray *)Q
{
    NSNumber *minDist = nil;
    NSUInteger index  = 0;
    
    for (Vertex *vertex in Q) {
        if (!vertex.wasVisited) {
            NSUInteger tmpIndex = vertex.index;
            if (!minDist) {
                minDist = [aDist    objectAtIndex:tmpIndex];
                index   = [Q        indexOfObject:vertex];
            }
            else {
                if ([minDist isGreaterThan:[aDist objectAtIndex:tmpIndex]]) {
                    minDist = [aDist    objectAtIndex:tmpIndex];
                    index   = [Q        indexOfObject:vertex];
                }
            }
        }
    }
    
    return Q[index];
}

- (Vertex *)existingVertexWithCoordinates: (NSArray*)aCoordinates {
    for (Vertex *vertex in self.vertices) {
        if (vertex.coordinates.longitude == [aCoordinates[0] doubleValue]
            && vertex.coordinates.latitude == [aCoordinates[1] doubleValue])
        {
            return vertex;
        }
    }
    return nil;
}


#pragma mark - A-Star


- (NSArray *)AStarShortestPathFromVertex:(Vertex *)aSource toVertex:(Vertex *)aTarget {
    Vertex *startNode = aSource;
    Vertex *endNode = aTarget;
    
    startNode.gScore = 0;
    startNode.speed  = 0;
    startNode.hScore = [Vertex distanceBetweenFirstCoordinate:startNode.coordinates
                                                    andSecond:endNode.coordinates];
    
    self.openList   = [[NSMutableArray alloc] initWithObjects:startNode, nil];
    NSMutableArray *closedList = [@[] mutableCopy];
    
    Vertex *current;
    Vertex *neighbour;
    while (self.openList.count) {
        current = [self cheapestNode];
        if(current == endNode) {
            //current.parent = neighbor;
            break;
        }
        
        [self.openList removeObject:current];
        [closedList addObject: current];
        
        for (Edge *edge in current.adjacentEdges) {
            neighbour = edge.adjacentTo;
            Float64 dist  = [Vertex distanceBetweenFirstCoordinate:neighbour.coordinates
                                                         andSecond:current.coordinates];
            Float64 tentScore = current.gScore + dist;
            neighbour.gScore = tentScore;
            
            
            if ([closedList containsObject:neighbour] && tentScore >= neighbour.gScore)
                continue;
            
            if (![closedList containsObject:neighbour] || tentScore < neighbour.gScore) {
                neighbour.parent = current;
                neighbour.gScore = tentScore;
                neighbour.hScore = [Vertex distanceBetweenFirstCoordinate:neighbour.coordinates
                                                                andSecond:endNode.coordinates];
                if(![self.openList containsObject:neighbour]) {
                    [self.openList addObject:neighbour];
                }
            }
        }
    }
    
    NSMutableArray *result = [NSMutableArray new];
    [Graph printChainFrom:startNode
                  endNode:endNode
                  toArray:result];
    [self resetVertices];
    return result;
}

+ (void)printChainFrom:(Vertex *)startNode
               endNode:(Vertex *)end
               toArray:(NSMutableArray *)arr  {
    
    if (end.index == startNode.index) {
        NSLog(@"Start node --> %ld", (long)end.index);
        [arr addObject:[NSNumber numberWithInteger:end.index]];
        return;
    }
    Vertex *previous = end.parent;
    if (previous) {
        [Graph printChainFrom:startNode
                      endNode:previous
                      toArray:arr];
        [arr addObject:[NSNumber numberWithInteger:end.index]];
        NSLog(@"next node --> %ld", (long)end.index);
    }
}

- (Vertex *) cheapestNode {
    Vertex *cheapest = [self.openList objectAtIndex:0];
    
    for (Vertex *node in self.openList) {
        if ([node fScore] < [cheapest fScore])
            cheapest = node;
    }
    
    return cheapest;
}


#pragma mark - Routes


- (void)coordinatesFromStartCoordinate:(CLLocationCoordinate2D)startCoordinate
                       toEndCoordinate:(CLLocationCoordinate2D)endCoordinate
                            completion:(void (^)(NSArray*))completion {
    __block NSArray *resultIndexes, *resultVertices, *resultCoordinates;
    Vertex *start, *end;
    start = [self nearestVertexWithCoordinate:startCoordinate];
    end = [self nearestVertexWithCoordinate:endCoordinate];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        resultIndexes = [self AStarShortestPathFromVertex:start toVertex:end];
        //resultIndexes = [self dijkstraShortestPathFromVertex:start toVertex:end];
        resultVertices = [self verticesWithIndexesArray:resultIndexes];
        resultCoordinates = [self roadCoordinatesWithVerticesArray:resultVertices];
        completion(resultCoordinates);
    });
}

- (NSArray *)roadCoordinatesWithVerticesArray:(NSArray *)aVerticesArr {
    NSMutableArray *route = [NSMutableArray array];
    for (Vertex *vertex in aVerticesArr) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:vertex.coordinates.latitude
                                                          longitude:vertex.coordinates.longitude];
        [route addObject:location];
    }
    return route;
}

- (NSArray *)verticesWithIndexesArray:(NSArray *)indexArr {
    NSMutableArray *verticesArr = [NSMutableArray array];;
    for (NSNumber *index in indexArr) {
        [verticesArr addObject:self.vertices[index.unsignedIntegerValue]];
    }
    return verticesArr;
}

- (Vertex *)nearestVertexWithCoordinate:(CLLocationCoordinate2D)coordinate {
    Float64 minDistance = @INT_MAX.doubleValue;
    Vertex *nearestVertex;
    for (Vertex *currentVertex in self.vertices) {
        Float64 weight = [Vertex weightFromVertex:currentVertex toCoordinate:coordinate];
        if (weight < minDistance) {
            minDistance = weight;
            nearestVertex = currentVertex;
        }
    }
    return nearestVertex;
}





@end

