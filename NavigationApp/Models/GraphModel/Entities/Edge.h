//
//  Edge.h
//  NavigationApp
//
//  Created by Alex on 18.03.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vertex.h"
@interface Edge : NSObject
@property (nonatomic, strong) Vertex *adjacentFrom;
@property (nonatomic, strong) Vertex *adjacentTo;
@property (nonatomic, strong) NSNumber *weight;
@property (nonatomic, assign) BOOL     used;

- (instancetype)initWithAdjacentFrom:(Vertex *)vertexFrom
                                  To:(Vertex *)vertexTo
                           andWeight:(NSNumber *)weight;
@end
