//
//  Node.h
//  NavigationApp
//
//  Created by Alex on 16.03.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Vertex : NSObject
@property (assign, nonatomic)   CLLocationCoordinate2D coordinates;
@property (assign, nonatomic)   NSInteger       index;
@property (assign, nonatomic)   BOOL            wasVisited;
@property (strong, nonatomic)   NSMutableSet    *adjacentEdges;
//AStar
@property (assign, nonatomic)   Float64 gScore;
@property (assign, nonatomic)   Float64 speed;
@property (assign, nonatomic)   Float64 hScore;
@property (strong, nonatomic)   Vertex *parent;

- (Float64)         fScore;
- (instancetype)    initWithCoordinates:(CLLocationCoordinate2D)aCoordinates;
- (void)            addEdgeToVertex:(Vertex *)aNextVertex;
+ (Float64)         weightFromVertex:(Vertex *)aVertex toCoordinate:(CLLocationCoordinate2D)aCoordinate;
+ (Float64)         distanceBetweenFirstCoordinate:(CLLocationCoordinate2D)first
                                         andSecond:(CLLocationCoordinate2D)second;
@end
