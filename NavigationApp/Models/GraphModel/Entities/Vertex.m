//
//  Node.m
//  NavigationApp
//
//  Created by Alex on 16.03.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "Vertex.h"
#import "Edge.h"

@implementation Vertex

- (instancetype)initWithCoordinates:(CLLocationCoordinate2D)aCoordinates {
    self = [super init];
    if(self) {
        self.coordinates = aCoordinates;
    }
    return self;
}

- (void)addEdgeToVertex:(Vertex *)aNextVertex {
    for (Edge *edge in self.adjacentEdges)
    {
        if(edge.adjacentTo == aNextVertex) {
            return;
        }
        
    }
    Float64 weight = [Vertex distanceBetweenFirstCoordinate:self.coordinates
                                               andSecond:aNextVertex.coordinates];
    [self.adjacentEdges addObject: [[Edge alloc] initWithAdjacentFrom:self
                                                                   To:aNextVertex
                                                            andWeight:@(weight)]];
}

+ (Float64)weightFromVertex:(Vertex *)aVertex toCoordinate:(CLLocationCoordinate2D)aCoordinate {
    return [Vertex distanceBetweenFirstCoordinate:aVertex.coordinates andSecond:aCoordinate];
}

+ (Float64) distanceBetweenFirstCoordinate:(CLLocationCoordinate2D)first andSecond:(CLLocationCoordinate2D)second {
    Float64 x = pow(first.latitude - second.latitude,2);
    Float64 y = pow(first.longitude - second.longitude,2);
    return sqrt(x+y);
}

- (BOOL) isEqualTo:(Vertex *)object
{
    if (self.coordinates.latitude == object.coordinates.latitude && self.coordinates.longitude == object.coordinates.longitude)
        return true;
    else
        return false;
}

-(NSMutableSet *)adjacentEdges {
    if(!_adjacentEdges) {
        _adjacentEdges = [NSMutableSet set];
    }
    return _adjacentEdges;
}

- (double)fScore {
    return self.hScore + self.gScore;
}

@end
