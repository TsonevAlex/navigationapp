//
//  Edge.m
//  NavigationApp
//
//  Created by Alex on 18.03.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "Edge.h"

@implementation Edge

- (instancetype)initWithAdjacentFrom:(Vertex *)vertexFrom
                                  To:(Vertex *)vertexTo
                           andWeight:(NSNumber *)weight
{
    self = [super init];
    
    if (!self) {
        return nil;
    }
    _adjacentFrom = vertexFrom;
    _adjacentTo   = vertexTo;
    _weight       = weight;
    _used         = NO;
    return self;
}

@end
