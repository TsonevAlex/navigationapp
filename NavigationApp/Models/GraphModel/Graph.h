//
//  PathFinder.h
//  NavigationApp
//
//  Created by Alex on 11.02.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vertex.h"
@import GoogleMaps;

@interface Graph : NSObject

@property (strong, nonatomic) NSMutableArray *vertices;

#pragma mark - Dijkstra

- (NSArray *)   dijkstraShortestPathFromVertex:(Vertex *)aSource toVertex:(Vertex *)aTarget;
- (Vertex *)    existingVertexWithCoordinates: (NSArray*)aCoordinates ;

#pragma mark - Routes

- (NSArray *)   verticesWithIndexesArray:(NSArray *)indexArr;
- (Vertex *)    nearestVertexWithCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)        coordinatesFromStartCoordinate:(CLLocationCoordinate2D)startCoordinate
                               toEndCoordinate:(CLLocationCoordinate2D)endCoordinate
                                    completion:(void (^)(NSArray*))completion;

@end


