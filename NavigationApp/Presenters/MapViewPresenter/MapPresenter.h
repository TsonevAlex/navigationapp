//
//  MapPresenter.h
//  NavigationApp
//
//  Created by Alex on 21.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Protocols.h"

@interface MapPresenter : NSObject <MapPresenterProtocol>

@property (strong, nonatomic) id<MapViewProtocol> viewController;

- (instancetype)initWithMapController:(id<MapViewProtocol>)mapViewController;

@end
