//
//  MapPresenter.m
//  NavigationApp
//
//  Created by Alex on 21.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "MapPresenter.h"
#import "GoogleAPIManager.h"
#import "GraphDataManager.h"
#import "Const.h"
#import "LoadingViewPresenter.h"
#import "Graph.h"

@interface MapPresenter ()


#pragma mark - Graph


@property (strong, nonatomic) Graph *graph;

@end

@implementation MapPresenter


#pragma mark - Life Cycle


- (instancetype)initWithMapController:(id<MapViewProtocol>)mapViewController {
    if(self = [super init]) {
        self.viewController = mapViewController;
        [self configureGraph];
    }
    return self;
}


#pragma mark - Graph


- (void)configureGraph {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        if ([GraphDataManager isGraphExists]) {
            self.graph = [GraphDataManager readFromJson];
        }
        else {
            LoadingViewPresenter *loading = [[LoadingViewPresenter alloc] init];
            [loading presentIn:((UIViewController *)self.viewController)];
            self.graph = [GraphDataManager calculateWithJson];
            [GraphDataManager writeToJson:self.graph withCompletion:^{
                [loading dismiss];
            }];
        }
    });
}


#pragma mark - MapPresenterProtocol


- (void)putMarkerAtCoordinate:(CLLocationCoordinate2D)coordinate {
    GMSMarker* marker = [GoogleAPIManager markerWithCoordinate:coordinate];
    [self.viewController setMarker:marker];
}

- (void)updateInfoWithStartCoordinate:(CLLocationCoordinate2D)start
                        endCoordinate:(CLLocationCoordinate2D)end {
    NSString *mode = [self defineTransportMode];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSDictionary *info = [GoogleAPIManager descriptionForPathWithStartCoordinate:start
                                                                       endCoordinate:end
                                                                             andMode:mode];
        NSString* time = info[kDuration];
        NSString* distance = info[kDistance];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.viewController updateInfoWithTime:time distance:distance];
        });
    });
}

- (void)drawPolylineWithStartCoordinate:(CLLocationCoordinate2D)start endCoordinate:(CLLocationCoordinate2D)end {
    [self.graph coordinatesFromStartCoordinate:start
                               toEndCoordinate:end
                                    completion:^(NSArray *array) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            GMSPolyline *polyline = [MapPresenter polylineWithLocationsArray:array];
                                            [self.viewController drawPolyline:polyline];
                                        });
                                    }];
}

+ (GMSPolyline *)polylineWithLocationsArray:(NSArray *)locationsArray  {
    GMSMutablePath *path = [GMSMutablePath path];
    
    for (CLLocation* location in locationsArray) {
        [path addCoordinate:location.coordinate];
    }
    GMSPolyline *result = [GMSPolyline polylineWithPath:path];
    return result;
}

- (NSString *)defineTransportMode {
    if (self.viewController.transportMode == 1) {
        return kTransportModeDriving;
    }
    else {
        return kTransportModeWalkng;
    }
}

@end
