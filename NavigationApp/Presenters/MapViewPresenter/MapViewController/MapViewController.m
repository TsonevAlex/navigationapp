//
//  ViewController.m
//  NavigationApp
//
//  Created by Alex on 11.02.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "MapViewController.h"
#import "InfoBarView.h"
#import "MapPresenter.h"
#import "Const.h"
#import <Masonry/Masonry.h>
#import <DGActivityIndicatorView.h>
@import GoogleMaps;

@interface MapViewController () <GMSMapViewDelegate, InfoBarDelegate>


#pragma mark - GMS


@property (strong, nonatomic) GMSMapView *mapView;
@property (strong, nonatomic) GMSMarker *startMarker;
@property (strong, nonatomic) GMSMarker *endMarker;
@property (strong, nonatomic) GMSPolyline *line;


#pragma mark - UI


@property (strong, nonatomic) InfoBarView *infoBar;
@property (strong, nonatomic) DGActivityIndicatorView *spinner;


@end

@implementation MapViewController


#pragma mark - Lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    [self confiureUI];
    [self.view setNeedsUpdateConstraints];
}


#pragma mark - GMSMapViewDelegate


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    if(self.spinner.isHidden) {
        [self removeAllSubviews];
        [self hideInfoBar];
    }
}

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    if(self.spinner.isHidden) {
        [self.presenter putMarkerAtCoordinate:coordinate];
    }
}


#pragma mark - InfoBarViewDelegate


- (void)transportModeChanged {
    [self updateInfo];
}


#pragma mark - UI


- (void)confiureUI {
    [self configureMapView];
    [self configureInfoBar];
    [self configureSpinner];
}

- (void)configureMapView {
    self.mapView = [[GMSMapView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.mapView];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:kDefaultLatitude
                                                            longitude:kDefaultLongtitude
                                                                 zoom:kDefaultCameraZoom];
    self.mapView.camera = camera;
    self.mapView.delegate = self;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight |
                                    UIViewAutoresizingFlexibleWidth;
}

- (void)configureInfoBar {
    self.infoBar = [[InfoBarView alloc] initWithFrame:CGRectZero
                                             delegate:self];
    [self.mapView addSubview:self.infoBar];
    [self setupInfoBarConstraints];
    
}

- (void)configureSpinner {
    self.spinner = [[DGActivityIndicatorView alloc]
                    initWithType:DGActivityIndicatorAnimationTypeBallScale
                    tintColor:[UIColor whiteColor]
                    size:50.0f];
    [self.view addSubview:self.spinner];
    [self setupSpinnerConstraints];
}

- (void)removeAllSubviews {
    [self removeStartMarker];
    [self removeEndMarker];
    [self removeLine];
    [self hideInfoBar];
}


- (void)updateInfo {
    [self.presenter updateInfoWithStartCoordinate:self.startMarker.position
                                    endCoordinate:self.endMarker.position];
}



#pragma mark - Constraints


- (void)setupInfoBarConstraints {
    __weak typeof(self.mapView) welf = self.mapView;
    [self.infoBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(welf.mas_bottom);
        make.bottom.equalTo(welf.mas_bottom);
        make.left.equalTo(welf.mas_left);
        make.right.equalTo(welf.mas_right);
    }];
}


- (void)hideInfoBar {
    __weak typeof(self.mapView) welf = self.mapView;
    [self.infoBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(welf.mas_bottom);
    }];
    [UIView animateWithDuration:0.3
                          delay:0.0
         usingSpringWithDamping:0.7
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                     }];
}

- (void)showInfoBar {
    __weak typeof(self.mapView) welf = self.mapView;
    [self.infoBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(welf.mas_bottom).offset(-80.0);
    }];
    [UIView animateWithDuration:0.3
                          delay:0.0
         usingSpringWithDamping:0.7
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                     }];
    
}

- (void)setupSpinnerConstraints {
    __weak typeof(self) welf = self;
    [self.spinner mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(welf.view.mas_centerX);
        make.centerY.equalTo(welf.view.mas_centerY);
    }];
}


#pragma mark - Markers


- (void)removeStartMarker {
    self.startMarker.map = nil;
    self.startMarker = nil;
}

- (void)removeEndMarker {
    self.endMarker.map = nil;
    self.endMarker = nil;
}

- (void)putStartMarker:(GMSMarker *)marker {
    self.startMarker = marker;
    self.startMarker.map = self.mapView;
    self.mapView.selectedMarker = self.startMarker;
}

- (void)putEndMarker:(GMSMarker *)marker {
    self.endMarker = marker;
    self.endMarker.map = self.mapView;
    self.mapView.selectedMarker = self.endMarker;
}


#pragma mark - Polyline


- (void)drawLine {
    [self.spinner startAnimating];
    [self.presenter drawPolylineWithStartCoordinate:self.startMarker.position
                                      endCoordinate:self.endMarker.position];
}

- (void)removeLine {
    self.line.map = nil;
    self.line = nil;
}


#pragma mark - MapViewProtocol


- (void)drawPolyline:(GMSPolyline *)polyline {
    self.line = polyline;
    [self.spinner stopAnimating];
    self.line.strokeWidth = 3;
    self.line.map = self.mapView;
}

- (void)setMarker:(GMSMarker *)marker {
    if(self.line) {
        [self removeAllSubviews];
        [self hideInfoBar];
    }
    if(!self.startMarker) {
        [self putStartMarker:marker];
    }
    else if(!self.endMarker) {
        [self putEndMarker:marker];
    }
    if(self.startMarker && self.endMarker) {
        [self drawLine];
        [self.presenter updateInfoWithStartCoordinate:self.startMarker.position
                                        endCoordinate:self.endMarker.position];
    }
}

- (void)updateInfoWithTime:(NSString *)time distance:(NSString *)distance {
    [self.infoBar updateWithDistance:distance duration:time];
    [self showInfoBar];
    
}

- (NSInteger)transportMode {
    return self.infoBar.transportMode;
}

@end
