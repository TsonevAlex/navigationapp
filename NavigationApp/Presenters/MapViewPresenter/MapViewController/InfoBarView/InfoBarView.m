//
//  InfoBarView.m
//  NavigationApp
//
//  Created by Alex on 04.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "InfoBarView.h"
#import "Protocols.h"
#import <Masonry/Masonry.h>
#import "DescriptionView.h"

@interface InfoBarView ()

@property (weak, nonatomic)   id<InfoBarDelegate> delegate;
@property (strong, nonatomic) UIStackView *stack;
@property (strong, nonatomic) DescriptionView *distanceView;
@property (strong, nonatomic) DescriptionView *durationView;
@property (strong, nonatomic) UISegmentedControl *transportModeView;

@end

@implementation InfoBarView


#pragma mark - Life Cycle


- (instancetype)initWithFrame:(CGRect)frame
                     delegate:(id<InfoBarDelegate>)delegate {
    if (self = [super initWithFrame:frame]) {
        self.delegate = delegate;
        [self setupUI];
        
    }
    return self;
}


#pragma mark - UI


- (void)setupUI {
    self.backgroundColor = [UIColor clearColor];
    self.effect = [[UIBlurEffect alloc] init];
    self.autoresizingMask = UIViewAutoresizingFlexibleHeight |
                            UIViewAutoresizingFlexibleWidth;
    self.layer.cornerRadius = 10.0;
    self.layer.masksToBounds = YES;
    [self setupSegmentedControl];
    [self setupStackView];    
}

- (void)setupSegmentedControl {
    self.transportModeView = [[UISegmentedControl alloc] initWithItems:@[@"Пешком",@"Авто"]];
    [self.transportModeView addTarget:self.delegate
                               action:@selector(transportModeChanged)
                     forControlEvents:UIControlEventValueChanged];
    self.transportModeView.selectedSegmentIndex = 0;
    
}

- (void)setupStackView {
    self.stack = [[UIStackView alloc] initWithFrame:self.bounds];
    self.stack.distribution = UIStackViewDistributionFillEqually;
    self.stack.alignment = UIStackViewAlignmentCenter;
    self.distanceView = [[DescriptionView alloc] initWithFrame:self.stack.bounds
                                                         title:@"Расстояние"];
    self.durationView = [[DescriptionView alloc] initWithFrame:self.stack.bounds
                                                         title:@"Длительность"];
    [self.stack addArrangedSubview:self.distanceView];
    [self.stack addArrangedSubview:self.transportModeView];
    [self.stack addArrangedSubview:self.durationView];
    [self addSubview:self.stack];
    [self setupStackViewConstraints];
}


#pragma mark - Controls


- (void)updateWithDistance:(NSString *)distance duration:(NSString *)duration {
    [self.distanceView updateValue:distance];
    [self.durationView updateValue:duration];
}

- (NSInteger)transportMode {
    return self.transportModeView.selectedSegmentIndex;
}

- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];
    [self.durationView updateValue:nil];
    [self.distanceView updateValue:nil];
}


#pragma mark - Constraints 


- (void)setupStackViewConstraints {
    __weak typeof(self) welf = self;
    [self.stack mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(welf);
        make.bottom.equalTo(welf);
        make.left.equalTo(welf);
        make.right.equalTo(welf);
    }];
}


@end
