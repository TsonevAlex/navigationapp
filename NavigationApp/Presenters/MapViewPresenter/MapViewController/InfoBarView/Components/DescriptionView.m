//
//  DescriptionView.m
//  NavigationApp
//
//  Created by Alex on 22.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "DescriptionView.h"
#import <Masonry/Masonry.h>

@interface DescriptionView ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *valueLabel;

@end

@implementation DescriptionView
//title:(NSString *)title value:(NSString *)value
//self.titleLabel.text = title;
//self.valueLabel.text = value;

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title {
    if(self = [super initWithFrame:frame]) {
        [self setupUI];
        self.titleLabel.text = title;
    }
    return self;
}


#pragma mark - Updating Info


- (void)updateValue:(NSString *)value {
    self.valueLabel.text = value;
}


#pragma mark - UI


- (void)setupUI {
    [self setupTitleView];
    [self setupValueView];
}

- (void)setupTitleView {
    self.titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
    self.titleLabel.textColor = [UIColor darkGrayColor];
    self.titleLabel.font = [UIFont systemFontOfSize:12.0];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
}

- (void)setupValueView {
    self.valueLabel = [[UILabel alloc] initWithFrame:self.bounds];
    self.valueLabel.textColor = [UIColor blackColor];
    self.valueLabel.font = [UIFont systemFontOfSize:24.0];
    self.valueLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.valueLabel];
}


#pragma mark - Constratints


- (void)updateConstraints {
    [self setupTitleConstraints];
    [self setupValueConstraints];
    [super updateConstraints];
}

- (void)setupValueConstraints {
    __weak typeof(self) welf = self;
    [self.valueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(welf.mas_top);
        make.width.equalTo(welf.mas_width);
        make.centerX.equalTo(welf.mas_centerX);
    }];
}

- (void)setupTitleConstraints {
    __weak typeof(self) welf = self;
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(welf.mas_bottomMargin).with.offset(8.0);
        make.top.equalTo(welf.mas_top).with.offset(40.0);
        make.width.equalTo(welf.mas_width);
        make.centerX.equalTo(welf.mas_centerX);
    }];
}

@end
