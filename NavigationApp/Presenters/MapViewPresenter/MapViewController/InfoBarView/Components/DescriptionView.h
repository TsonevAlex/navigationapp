//
//  DescriptionView.h
//  NavigationApp
//
//  Created by Alex on 22.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionView : UIView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;
- (void)updateValue:(NSString *)value;

@end
