//
//  InfoBarView.h
//  NavigationApp
//
//  Created by Alex on 04.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Protocols.h"

@interface InfoBarView : UIVisualEffectView

@property (assign, nonatomic, readonly) NSInteger transportMode;

- (void)updateWithDistance:(NSString *)distance
                  duration:(NSString *)duration;
- (instancetype)initWithFrame:(CGRect)frame
                     delegate:(id<InfoBarDelegate>)delegate;


@end
