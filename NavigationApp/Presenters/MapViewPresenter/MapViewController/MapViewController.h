//
//  ViewController.h
//  NavigationApp
//
//  Created by Alex on 11.02.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Protocols.h"

@interface MapViewController : UIViewController <MapViewProtocol>

@property (strong, nonatomic) id<MapPresenterProtocol> presenter;
@property (assign, nonatomic, readonly) NSInteger transportMode;

@end

