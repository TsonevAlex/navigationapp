//
//  LoadingViewPresenter.m
//  NavigationApp
//
//  Created by Alex on 23.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "LoadingViewPresenter.h"
#import "LoadingViewController.h"

@interface LoadingViewPresenter ()

@property (strong, nonatomic) LoadingViewController* loadingViewController;

@end

@implementation LoadingViewPresenter

- (void)presentIn:(UIViewController *)viewController {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.loadingViewController = [[LoadingViewController alloc] init];
        [viewController presentViewController:self.loadingViewController
                                     animated:YES
                                   completion:nil];
    });
}

- (void)dismiss {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.loadingViewController dismissViewControllerAnimated:YES
                                                       completion:nil];
    });
}

@end
