//
//  LoadingViewPresenter.h
//  NavigationApp
//
//  Created by Alex on 23.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingViewPresenter : NSObject
- (void)presentIn:(UIViewController *)viewController;
- (void)dismiss;
@end
