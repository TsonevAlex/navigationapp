//
//  LoadingViewController.m
//  NavigationApp
//
//  Created by Alex on 23.04.17.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "LoadingViewController.h"
#import "Const.h"
#import <Masonry.h>
#import <DGActivityIndicatorView.h>

@interface LoadingViewController ()

@property (strong, nonatomic) DGActivityIndicatorView *indicatorView;
@property (strong, nonatomic) UILabel *descriptionView;
@property (strong, nonatomic) UILabel *authorView;

@end

@implementation LoadingViewController


#pragma mark - Life Cycle


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.964
                                                green:0.886
                                                 blue:0.964
                                                alpha:1.0];
    [self setupAnimation];
    [self setupDescription];
    [self setupAuthor];
    [self.view setNeedsUpdateConstraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UI


- (void)setupAnimation {
    self.indicatorView = [[DGActivityIndicatorView alloc]
                          initWithType:DGActivityIndicatorAnimationTypeBallScaleMultiple
                          tintColor:[UIColor blueColor]
                          size:100.0f];
    [self.view addSubview:self.indicatorView];
    [self.indicatorView startAnimating];
    [self setupIndicatorConstraints];
}

- (void)setupDescription {
    self.descriptionView = [[UILabel alloc] initWithFrame:CGRectZero];
    self.descriptionView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0];
    self.descriptionView.text = kDescriptionText;
    self.descriptionView.textColor = [UIColor grayColor];
    [self.view addSubview:self.descriptionView];
    [self setupDescriptionConstraints];
}

- (void)setupAuthor {
    self.authorView = [[UILabel alloc] initWithFrame:CGRectZero];
    self.authorView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    self.authorView.text = kAuthorText;
    self.authorView.textColor = [UIColor lightGrayColor];
    [self.view addSubview:self.authorView];
    [self setupAuthorConstraints];
}


#pragma mark - Constraints


- (void)setupIndicatorConstraints {
    __weak typeof(self) welf = self;
    [self.indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(welf.view.mas_centerX);
        make.centerY.equalTo(welf.view.mas_centerY).offset(-50.0);
    }];
}

- (void)setupDescriptionConstraints {
    __weak typeof(self) welf = self;
    [self.descriptionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(welf.view.mas_centerX);
        make.top.equalTo(welf.indicatorView.mas_bottom).offset(10.0);
    }];
}

- (void)setupAuthorConstraints {
    __weak typeof(self) welf = self;
    [self.authorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(welf.view.mas_centerX);
        make.bottom.equalTo(welf.view.mas_bottom).offset(-10.0);
    }];
}

@end
