# README #

Application is made as a university project. The aim was to implement shortest-path finding algorithms, so Dijkstra and A-Star was implemented. 
OpenStreetMap road extracts were used as initial information. GEOJSON file was cleaned from unnecessary nodes, parsed and written to another JSON, that contains only processed values.

### How do I get set up? ###

You should have CocoaPods installed to launch that project.
PodInit - attached.


```
#!

target ‘NavigationApp’ do
  pod 'GoogleMaps'
  pod 'Masonry'
  pod 'DGActivityIndicatorView'
end
```

### Guidelines ###

First launch is performance expensive. The graph will be calculated (~6700 vertices).

* Network connection required.
* First long press puts a start marker.
* Second - an end marker.
* Short tap resets map state.